package config

import (
	"fmt"
	"os"
	"time"
)

func PrintLog(title string, message string) {
	dt := time.Now()
	var f *os.File
	var err error
	// If the file doesn't exist, create it, or append to the file

	f, err = os.OpenFile("logs/"+dt.Format("20060102")+".txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		f, err = os.OpenFile("../logs/"+dt.Format("20060102")+".txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Println("Failed to create log")
			return
		}
	}

	_, errWrite := f.Write([]byte(dt.Format("2006-01-02 15:04:05") + " [" + title + "]\n" + message + "\n\n"))
	if errWrite != nil {
		f.Close() // ignore error; Write error takes precedence
		fmt.Println(errWrite)
	}
	if err := f.Close(); err != nil {
		fmt.Println(err)
	}
}
