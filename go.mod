module moshub-consumer-go-kalcare

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/rs/zerolog v1.23.0
	github.com/segmentio/kafka-go v0.4.17
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
)
