package service

import (
	"moshub-consumer-go-kalcare/repository"
)

func NewKalcareService(prodRepo repository.ProductRepository) KalcareService { // Untuk melakukan init koneksi
	return &kalcareServiceImpl{
		prodRepo: prodRepo,
	}
}

type kalcareServiceImpl struct {
	prodRepo repository.ProductRepository
}
