package service

import (
	"moshub-consumer-go-kalcare/model"
	"moshub-consumer-go-kalcare/repository"
)

type KalcareService interface {
	UpdateStockProductKalcare(shop_id_partner string, sku_partner string, repo repository.ProductRepository, data model.MasterProductUser)
	UpdateStockAllProductKalcare(shop_id_partner string, req_body string, repo repository.ProductRepository)
	//UpdateStockProductKalcare2(email string, password string, shop_id_partner string, repo repository.ProductRepository, data model.RequestUpdateStock)
	//RequestUpdateStockKalcare(body string, token string, shop_id_partner string)
	Auth(email string, password string) string
	UpdateTesting()
	//UpdateProductByName2(data model.MasterProductUser, shopId int)
}
