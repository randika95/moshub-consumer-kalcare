package service

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"moshub-consumer-go-kalcare/model"
	"moshub-consumer-go-kalcare/repository"
	"net/http"
	"strings"
	"time"
)

var baseURL string = "https://kalcare-api.karsalintasbuwana.com/"

func (repository *kalcareServiceImpl) UpdateStockKalcare(data model.MasterProductUser) {
	req_tokenIT := model.TokenIT{{
		SKU:       "LMAM1",
		New_Stock: 13,
	},
	}

	body, err := json.Marshal(req_tokenIT)
	fmt.Println(body)

	req, err := http.NewRequest("POST", "https://kalcare-api.karsalintasbuwana.com/master/orderStatuses", strings.NewReader(""))
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("Accept", "application/json")
	sign := "Bearer c:6jgrJbtHQLK1gjlnc1Ebiw"
	req.Header.Add("Authorization", sign)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(string(b))

}

func (repository *kalcareServiceImpl) UpdateTesting() {

	url := "https://accounts.tokopedia.com/token?grant_type=client_credentials"
	method := "POST"
	encodedString := base64.StdEncoding.EncodeToString([]byte("19fed073757f4e4e98d26ba15ea11cc6" + ":" + "91de754100014eb2be46915c7100d92b"))
	//var resp = model.ResponseGenerateTokenTokopedia{}

	request, err := http.NewRequest(method, url, strings.NewReader(""))
	if err != nil {
		fmt.Println(err)
	}

	request.Header.Add("Authorization", "Basic "+encodedString)
	request.Header.Add("Content-Length", "0")
	request.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(request)

	if response != nil {
		defer response.Body.Close()
	}
	if err != nil {
		fmt.Println(err)
	}

	token_tokped, err := io.ReadAll(response.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Token adalah :")
	fmt.Println(string(token_tokped))

	req_tokenIT := model.TokenIT{{
		SKU:       "LMAM1",
		New_Stock: 150,
	},
	}

	body, err := json.Marshal(req_tokenIT)

	req, err := http.NewRequest("POST", "https://fs.tokopedia.net/inventory/v1/fs/14914/stock/update?shop_id=11227672&warehouse_id=12505483", strings.NewReader(string(body)))
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(string(body))
	req.Header.Set("Accept", "application/json")
	sign := "Bearer c:kMBlSe2LQvudYmWCfUnyRw"
	req.Header.Add("Authorization", sign)

	client_tokped := &http.Client{}
	resp, err := client_tokped.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	b2, err := io.ReadAll(resp.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err, b2)
	}

	fmt.Println(string(b2))
	//fmt.Println("{ 'header': { 'process_time': 0.045122173, 'messages': 'Your request has been processed successfully' }, 'data': { 'failed_rows': 0, 'failed_rows_data': [], 'succeed_rows': 3 } }")

}

func RequestUpdateStockKalcare(body string, shop_id_partner string, repo repository.ProductRepository) (string, string) {

	data_response := model.ResponseUpdateStock{}
	//fmt.Println("Test 123")
	//fmt.Println(string(body))
	req, err := http.NewRequest("POST", "https://kalcare-api.karsalintasbuwana.com/merchant/product/merchantstock/"+shop_id_partner, strings.NewReader(string(body)))

	if err != nil {
		fmt.Println("Error data")
	}

	req.Header.Set("Accept", "application/json")
	sign := "Bearer " + model.Token_access[0]
	req.Header.Add("Authorization", sign)
	client_tokped := &http.Client{}
	resp, err := client_tokped.Do(req)
	if err != nil {
		fmt.Println("Error")
		//log.Fatalln(err)
	}

	defer resp.Body.Close()
	respon_api, _ := io.ReadAll(resp.Body)

	json.Unmarshal([]byte(string(respon_api)), &data_response)
	reqHeadersBytes, _ := json.Marshal(req.Header)
	header := string(reqHeadersBytes)

	return string(respon_api), string(header)
}

func RequestUpdateStockKalcare2(body string, shop_id_partner string, repo repository.ProductRepository) (string, string) {
	data_response := model.ResponseUpdateStock{}
	//fmt.Println("Test 123")
	//fmt.Println(string(body))
	req, err := http.NewRequest("POST", "https://kalcare-api.karsalintasbuwana.com/merchant/product/merchantstock/"+shop_id_partner, strings.NewReader(body))

	if err != nil {
		fmt.Println("Error data")
	}

	req.Header.Set("Accept", "application/json")
	sign := "Bearer " + model.Token_access[0]
	req.Header.Add("Authorization", sign)
	client_tokped := &http.Client{}
	resp, err := client_tokped.Do(req)
	if err != nil {
		fmt.Println("Error")
		//log.Fatalln(err)
	}

	defer resp.Body.Close()
	respon_api, _ := io.ReadAll(resp.Body)

	json.Unmarshal([]byte(respon_api), &data_response)

	reqHeadersBytes, _ := json.Marshal(req.Header)
	header := string(reqHeadersBytes)

	return string(respon_api), string(header)
}

func (repository *kalcareServiceImpl) UpdateStockProductKalcare(shop_id_partner string, sku_partner string, repo repository.ProductRepository, data_product model.MasterProductUser) {

	start := time.Now()
	items := []model.Merchand{}
	productList := model.Merchand{Product_sku: sku_partner, Quantity: data_product.Qty}
	items = append(items, productList)

	req_body := &model.RequestUpdateStock{
		MerchantProduct: append([]model.Merchand{}, items...),
	}

	body, _ := json.Marshal(req_body)
	// update stock to kalcare
	data_response, header := RequestUpdateStockKalcare(string(body), shop_id_partner, repo)
	elapsed := (time.Since(start) / time.Millisecond)
	fmt.Println(data_response)
	repo.InsertLogMarketplace("CONSUMER_KALCARE_UPDATE_STOCK", header, string(body), "Response Moshub", elapsed, time.Now(), "MOSHUB_CONSUMER_KALCARE", time.Now(), "MOSHUB_CONSUMER_KALCARE", data_response)

}

func (repository *kalcareServiceImpl) UpdateStockAllProductKalcare(shop_id_partner string, req_body string, repo repository.ProductRepository) {

	data_response, header := RequestUpdateStockKalcare2(req_body, shop_id_partner, repo)
	fmt.Println(data_response)

	// update stock All product every 6 am and 7 pm to kalcare
	start := time.Now()
	elapsed := (time.Since(start) / time.Millisecond)

	fmt.Println(shop_id_partner, data_response, elapsed)
	repo.InsertLogMarketplace("CONSUMER_KALCARE_UPDATE_ALL_STOCK", header, req_body, "Response Moshub", elapsed, time.Now(), "MOSHUB_CONSUMER_KALCARE", time.Now(), "MOSHUB_CONSUMER_KALCARE", data_response)

}

func (repository *kalcareServiceImpl) Auth(email string, password string) string {
	data_response := model.ResponseGenerateTokenKalcare{}

	req_tokenIT := model.AuthToken{
		Email:    email,
		Password: password,
	}

	body, err := json.Marshal(req_tokenIT)

	req, err := http.NewRequest("POST", "https://kalcare-api.karsalintasbuwana.com/auth/login", strings.NewReader(string(body)))
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("Accept", "application/json")
	client_tokped := &http.Client{}
	resp, err := client_tokped.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err, data)
	}
	json.Unmarshal([]byte(data), &data_response)
	return data_response.Access_token

}

/*
func (repository *kalcareServiceImpl) UpdateStockProductKalcare2(email string, password string, shop_id_partner string, repo repository.ProductRepository, req_tokenIT model.RequestUpdateStock) {
	start := time.Now()
	data_response := model.ResponseUpdateStock{}
	if len(model.Token_access) == 0 {
		token := repository.Auth(email, password)
		model.Token_access = append(model.Token_access, token)
	}

	body, _ := json.Marshal(req_tokenIT)
	req, err := http.NewRequest("POST", "https://kalcare-api.karsalintasbuwana.com/merchant/product/merchantstock/"+shop_id_partner, strings.NewReader(string(body)))

	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("Accept", "application/json")
	sign := "Bearer " + model.Token_access[0]
	req.Header.Add("Authorization", sign)
	client_kalcare := &http.Client{}
	resp, err := client_kalcare.Do(req)
	if err != nil {
		elapsed := (time.Since(start) / time.Millisecond)
		repo.InsertLogMarketplace("CONSUMER_KALCARE", "", 200, "body", "ERROR", "IP", elapsed, time.Now(), "MOSHUB_CONSUMER_KALCARE", time.Now(), "MOSHUB_CONSUMER_KALCARE")

	}

	defer resp.Body.Close()

	respon_api, _ := io.ReadAll(resp.Body)
	json.Unmarshal([]byte(respon_api), &data_response)

	elapsed := (time.Since(start) / time.Millisecond)
	if data_response.MerchandProduct.Message != "success" {
		repo.InsertLogMarketplace("CONSUMER_KALCARE", "", 200, "body", data_response.MerchandProduct.Message, "IP", elapsed, time.Now(), "MOSHUB_CONSUMER_KALCARE", time.Now(), "MOSHUB_CONSUMER_KALCARE")

	} else {
		repo.InsertLogMarketplace("CONSUMER_KALCARE", "", 200, "body", data_response.MerchandProduct.Message, "IP", elapsed, time.Now(), "MOSHUB_CONSUMER_KALCARE", time.Now(), "MOSHUB_CONSUMER_KALCARE")
	}

}
*/
