package main

import (
	"encoding/json"
	"moshub-consumer-go-kalcare/model"
	"moshub-consumer-go-kalcare/repository"
	"moshub-consumer-go-kalcare/service"
)

func consume(repo repository.ProductRepository, data model.ProductConsumer) {
	kalcareService2 := service.NewKalcareService(productRepository)
	sku_partner, shop_id_partner, _, _ := repo.GetSKU_Shop_id_partner(data.After.Id_user, data.After.Sku)
	if sku_partner != "" && shop_id_partner != "" {
		kalcareService2.UpdateStockProductKalcare(shop_id_partner, sku_partner, repo, data.After)

	}

}

func consume_all_product(repo repository.ProductRepository) {
	kalcareService2 := service.NewKalcareService(productRepository)
	data_all_product, _ := repo.Get_Product_Kalcare("4")
	stock := model.RequestUpdateStock{}
	var i int
	for i = 0; i < len(data_all_product); i++ {
		json.Unmarshal([]byte(data_all_product[i].Product), &stock.MerchantProduct)
		user := &stock
		b, _ := json.Marshal(user)

		kalcareService2.UpdateStockAllProductKalcare(data_all_product[i].Shop_id_partner, string(b), productRepository)
	}
}
