package db

import (
	"os"
	"strings"

	"github.com/segmentio/kafka-go"
)

var r *kafka.Reader

// InitRedis initialize redis connection.
func InitKafka() {
	brokers := strings.Split(os.Getenv("KAFKA_HOST"), ",")
	// fmt.Println(brokers)

	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: brokers,
		// GroupID: "test-consumer-1",
		// Topic:   "img_marketplace_testing",
		Topic:   "product_testing",
		GroupID: "moshub-consumer-kalcare",
	})

	r = reader
}

// GetRedis return redis client.
func GetKafka() *kafka.Reader {
	return r
}
