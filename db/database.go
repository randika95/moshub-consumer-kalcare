package db

import (
	"fmt"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	DB    *gorm.DB
	err   error
	DBErr error
)

// type Database struct {
// 	*gorm.DB
// }

// Setup opens a database and saves the reference to `Database` struct.
func Setup() {
	var db = DB

	database := os.Getenv("DB_DATABASE")
	username := os.Getenv("DB_USERNAME")
	password := os.Getenv("DB_PASSWORD")
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")

	// fmt.Println(database, username, password, host, port)

	db, err = gorm.Open("postgres", "host="+host+" port="+port+" user="+username+" dbname="+database+"  sslmode=disable password="+password)
	if err != nil {
		DBErr = err
		fmt.Println("db err: ", err)
	}

	// Change this to true if you want to see SQL queries
	//db.LogMode(config.Database.LogMode)

	//
	db.DB().SetMaxOpenConns(10)
	db.DB().SetMaxIdleConns(5)
	db.DB().SetConnMaxLifetime(1 * time.Minute)

	// Auto migrate project models
	// db.AutoMigrate(&model.Post{}, &model.Tag{})
	DB = db
}

// GetDB helps you to get a connection
func GetDB() *gorm.DB {
	return DB
}

// GetDBErr helps you to get a connection
func GetDBErr() error {
	return DBErr
}
