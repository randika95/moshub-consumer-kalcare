package model

type ResponseSendMessage struct {
	Ok     int64             `json:"ok"`
	Result SendMessageResult `json:"result"`
}

type SendMessageResult struct {
	Message_id int32       `json:"message_id"`
	Result     interface{} `json:"result"`
	Chat       interface{} `json:"chat"`
	Date       int32       `json:"date"`
	Text       int32       `json:"text"`
}
