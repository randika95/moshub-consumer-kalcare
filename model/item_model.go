package model

type ProductConsumer struct {
	Before MasterProductUser `json:"before"`
	After  MasterProductUser `json:"after"`
	Op     string            `json:"op"`
	Ts_ms  int64             `json:"ts_ms"`
}

type MasterProductUser struct {
	Id                 int    `json:"id"`
	Id_user            int    `json:"id_user"`
	Product_name       string `json:"product_name"`
	Sku                string `json:"sku"`
	Qty                int    `json:"qty"`
	Price              string `json:"price"`
	Weight             string `json:"weight"`
	Weight_unit        string `json:"weight_unit"`
	Length             string `json:"length"`
	Width              string `json:"width"`
	Height             string `json:"height"`
	Desc               string `json:"desc"`
	Status             string `json:"status"`
	Product_name_store string `json:"product_name_store"`
	Update_date        int64  `json:"update_date"`
}

type MasterProductUserStore struct {
	Id                 int    `json:"id"`
	Id_user            int    `json:"id_user"`
	Product_name       string `json:"product_name"`
	Sku                string `json:"sku"`
	Sku_partner        string `json:"sku_partner"`
	Qty                int    `json:"qty"`
	Price              string `json:"price"`
	Weight             string `json:"weight"`
	Weight_unit        string `json:"weight_unit"`
	Length             string `json:"length"`
	Width              string `json:"width"`
	Height             string `json:"height"`
	Desc               string `json:"desc"`
	Status             string `json:"status"`
	Product_name_store string `json:"product_name_store"`
	Update_date        int64  `json:"update_date"`
}

type RequestUpdateProduct struct {
	Id                 int    `json:"id"`
	Id_user            int    `json:"id_user"`
	Product_name       string `json:"product_name"`
	Sku                string `json:"sku"`
	Qty                int    `json:"qty"`
	Price              string `json:"price"`
	Weight             string `json:"weight"`
	Weight_unit        string `json:"weight_unit"`
	Length             string `json:"length"`
	Width              string `json:"width"`
	Height             string `json:"height"`
	Desc               string `json:"desc"`
	Status             string `json:"status"`
	Product_name_store string `json:"product_name_store"`
}
