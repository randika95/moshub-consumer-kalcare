package model

/* var Token_access []struct {
	data string
} */

var Token_access []string

type KalcareAuth struct {
	Email    int64  `json:"email"`
	Password string `json:"password"`
}

type TokenIT []struct {
	SKU       string `json:"sku"`
	New_Stock int    `json:"new_stock"`
}

type AuthToken struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ResponseGenerateTokenKalcare struct {
	Meta         meta   `json:"meta"`
	Access_token string `json:"access_token"`
	Expires      int32  `json:"expires"`
	Token_type   string `json:"token_type"`
}

type ResponseUpdateStock struct {
	Meta            meta2     `json:"meta"`
	MerchandProduct Merchand2 `json:"merchantProduct"`
}

type meta2 struct {
	Json_api  version `json:"jsonapi"`
	Base_url  string  `json:"base_url"`
	Self      string  `json:"self"`
	Params    params  `json:"params"`
	Timestamp string  `json:"timestamp"`
}

type params struct {
	Merchand_code string `json:"merchand_code"`
}

type RequestUpdateStock struct {
	MerchantProduct []Merchand `json:"merchantProduct"`
}
type Merchand2 struct {
	Product_sku string `json:"product_sku"`
	Quantity    int    `json:"quantity"`
	Message     string `json:"message"`
}

type Merchand struct {
	Product_sku string `json:"product_sku"`
	Quantity    int    `json:"quantity"`
}

type meta struct {
	Json_api  version `json:"jsonapi"`
	Base_url  string  `json:"base_url"`
	Self      string  `json:"self"`
	Params    string  `json:"params"`
	Timestamp string  `json:"timestamp"`
}

type version struct {
	Version string `json:"version"`
}

type All_product struct {
	Id_user         string `json:"id_user"`
	Id_marketplace  string `json:"id_marketplace"`
	Shop_id_partner string `json:"shop_id_partner"`
	Product         string `json:"product"`
}

type All_product_details struct {
	Id_user         string     `json:"id_user"`
	Shop_id_partner string     `json:"shop_id_partner"`
	Stock_product   []Merchand `json:"stock_product"`
}

// type ConsumerTest struct {
// 	Sku       string   `json:"sku"`
// 	Imgs      []string `json:"imgs"`
// 	PartnerId string   `json:"partnerId"`
// 	Timestamp int64    `json:"timestamp"`
// }
