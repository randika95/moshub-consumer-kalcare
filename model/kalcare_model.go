package model

type UpdateStockProduct struct {
	MerchantProduct []Product `json:"merchantproduct"`
}

type Product struct {
	Product_sku string `json:"product_sku"`
	Quantity    string `json:"quantity"`
}

type ResponseGenerateTokenkalcare struct {
	Access_token string `json:"access_token"`
	Expires_in   int32  `json:"expires_in"`
	Token_type   string `json:"token_type"`
}

type RequestCreateProduct struct {
	Products []CreateProduct `json:"products"`
}

type CreateProduct struct {
	Name           string `json:"name"`
	Category_id    int64  `json:"category_id"`
	Price          int    `json:"price"`
	Price_currency string `json:"price_currency"`
	Status         string `json:"status"`
	Min_order      int    `json:"min_order"`
	Weight         int    `json:"weight"`
	Weight_unit    string `json:"weight_unit"`
	Condition      string `json:"condition"`
	Description    string `json:"description"`
	Sku            string `json:"sku"`
	Stock          int    `json:"stock"`
}
