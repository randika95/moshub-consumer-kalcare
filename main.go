package main

import (
	"context"
	"encoding/json"
	"fmt"
	"moshub-consumer-go-kalcare/db"
	"moshub-consumer-go-kalcare/model"
	"moshub-consumer-go-kalcare/repository"
	"moshub-consumer-go-kalcare/service"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

var (
	productRepository repository.ProductRepository
)

func main() {

	//kalcareService := repository.NewProductRepository(db.DB)
	err := godotenv.Load()
	if err != nil {
		panic("No .env file specified")
		// fmt.Println("No .env file specified")
	}

	db.Setup()
	db.InitKafka()
	//db.InitRedis()
	r := db.GetKafka()
	/*redisClient = db.GetRedis()
	redisService = service.NewRedisService(redisClient)
	db := db.GetDB()
	*/
	//productRepository = repository.NewProductRepository(db)
	fmt.Println("Starting Consumer")

	//fmt.Println(time.Now().Minute())
	//handleRequests()
	kalcareService2 := service.NewKalcareService(productRepository)

	for {

		d, err := r.ReadMessage(context.Background()) // ReadMessage blocks until recieve new message
		if err != nil {
			log.Error().Msgf("error while receiving message: %s", err.Error())
			continue // skip below execution and proceed to new loop
		}

		objs := model.ProductConsumer{}
		errs := json.Unmarshal(d.Value, &objs)
		if errs != nil {
			log.Error().Msgf("error while converting message: %s", errs)
			continue
		}
		kalcareService := repository.NewProductRepository(db.DB)

		if len(model.Token_access) == 0 {
			email, password, _ := kalcareService.GetInfoAuth("auth_email_kalcare", "auth_password_kalcare")
			token := kalcareService2.Auth(email, password)
			model.Token_access = append(model.Token_access, token)

		}

		consume(kalcareService, objs)

	}

}
