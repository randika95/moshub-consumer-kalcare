package repository

import (
	"context"
	"errors"
	"moshub-consumer-go-kalcare/model"
	"time"

	"github.com/jinzhu/gorm"
)

type ProductRepositoryImpl struct {
	DB *gorm.DB
}

func NewProductRepository(db *gorm.DB) ProductRepository { // Untuk melakukan init koneksi
	return &ProductRepositoryImpl{
		DB: db,
	}
}

func (repository *ProductRepositoryImpl) Get_Product_Kalcare(id_marketplace string) ([]model.All_product, error) {
	ctx := context.Background()
	sql := `
	select mu.id as "moshub_shop_id", um.shop_id_partner as "moshub_shop_id_partner", um.id_marketplace ,
	(select 
	array_to_json(array_agg(row_to_json(t))) as products
	from 
	(
	select 
	mps.sku_partner as "product_sku", mpu.qty as "quantity" 
	from 
	master_product_user mpu 
	inner join 
	mapping_product_store mps on mps.moshub_shop_id = mpu.id_user and mpu.sku = mps.sku_epm 
	where 
	mpu.id_user = mu.id 
	)t)
	from 
	master_user mu
	inner join user_marketplace um on um.id_user = mu.id
	inner join master_marketplace mm on mm.id = um.id_marketplace 
	where
	um.id_marketplace = $1
	and 
	um.status = $2
	and 
	mu.status = $3
	and 
	um.shop_id_partner notnull
	`

	rows, err_sku := repository.DB.DB().QueryContext(ctx, sql, id_marketplace, "A", "A")
	var product_kalcare model.All_product
	var product_kalcare2 []model.All_product

	if err_sku != nil {
		return []model.All_product{}, err_sku
	}
	defer rows.Close()
	var i = 0

	for rows.Next() {
		//fmt.Println(rows.Scan())
		err := rows.Scan(&product_kalcare.Id_user, &product_kalcare.Shop_id_partner, &product_kalcare.Id_marketplace, &product_kalcare.Product)
		if err != nil {
			return []model.All_product{}, err
		}
		product_kalcare2 = append(product_kalcare2, product_kalcare)
		i = i + 1

	}

	return product_kalcare2, nil
}

func (repository *ProductRepositoryImpl) GetSKU_Shop_id_partner(id_user int, sku_enseval string) (string, string, error, error) {
	//fmt.Println("Tes 123")
	ctx := context.Background()
	//fmt.Println(id_user)
	//fmt.Println(sku_enseval)

	var data_sku_partner string
	sql := `select mps.sku_partner from master_product_user mpu inner join mapping_product_store mps on mps.sku_epm =mpu.sku where mpu.id_user =mps.moshub_shop_id and mpu.id_user =$1 and mpu.sku=$2`

	rows, err_sku := repository.DB.DB().QueryContext(ctx, sql, id_user, sku_enseval)

	if err_sku != nil {
		return "", "", err_sku, nil
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&data_sku_partner)
		if err != nil {
			return "", "", nil, nil
		}

	}

	//	fmt.Println("Test 12334", data_sku_partner)

	var shop_id_partner_marketplace string
	sql_shop := `select shop_id_partner from user_marketplace um where um.id_user =$1 and id_marketplace =$2`

	rows, err_shop := repository.DB.Raw(sql_shop, id_user, 4).Rows() // bisa pake ini juga

	if err_shop != nil {
		return "", "", err_shop, nil
	}
	defer rows.Close()

	if rows.Next() {
		err_shop := rows.Scan(&shop_id_partner_marketplace)
		if err_shop != nil {
			return "", "", err_shop, nil
		}
	} else {
		return "", "", err_shop, nil
	}

	return data_sku_partner, shop_id_partner_marketplace, nil, nil
}

func (repository *ProductRepositoryImpl) GetInfoAuth(email string, password string) (string, string, error) {
	var email_kalcare string
	var password_kalcare string

	sql := `select note from note where note_type = $1`

	rows, err := repository.DB.Raw(sql, email).Rows() // bisa pake ini juga

	if err != nil {
		return "", "", err
	}
	defer rows.Close()

	if rows.Next() {
		err := rows.Scan(&email_kalcare)
		if err != nil {
			return "", "", errors.New("error on query")
		}
	} else {
		return "", "", errors.New("data not found")
	}

	sql2 := `select note from note where note_type = $1`

	rows2, err2 := repository.DB.Raw(sql2, password).Rows() // bisa pake ini juga

	if err2 != nil {
		return "", "", err2
	}
	defer rows2.Close()

	if rows2.Next() {
		err2 := rows2.Scan(&password_kalcare)
		if err2 != nil {
			return "", "", errors.New("error on query")
		}
	} else {
		return "", "", errors.New("data not found")
	}

	return email_kalcare, password_kalcare, nil
}

func (repository *ProductRepositoryImpl) InsertLogMarketplace(api_name string, header string, body string, response string, respon_time_in_sec time.Duration, create_date time.Time, create_by string, update_date time.Time, update_by string, respon_marketplace string) error {
	sql := "INSERT INTO log_moshub_open_api (api_name, header, body, response, response_time_in_sec, create_date, create_by, update_date, update_by, response_marketplace) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"
	result := repository.DB.Exec(sql, api_name, header, body, response, respon_time_in_sec, create_date, "MOSHUB_KALCARE_CONSUMER", update_date, "MOSHUB_KALCARE_CONSUMER", respon_marketplace)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
