package repository

import (
	"moshub-consumer-go-kalcare/model"
	"time"
)

type ProductRepository interface {
	//=======================================================
	// PRODUCT
	//=======================================q================

	GetInfoAuth(email string, password string) (string, string, error)
	GetSKU_Shop_id_partner(id_user int, sku_enseval string) (string, string, error, error)
	Get_Product_Kalcare(id_marketplace string) ([]model.All_product, error)

	//=======================================================
	// LOG
	//=======================================================
	InsertLogMarketplace(api_name string, header string, body string, response string, respon_time_in_sec time.Duration, create_date time.Time, create_by string, update_date time.Time, update_by string, response_marketplace string) error
}
